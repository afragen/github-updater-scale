<?php
/**
 * Plugin Name: GitHub Updater at Scale
 * Plugin URI: https://bitbucket.org/afragen/github-updater-scale
 * Description: This plugin is used for testing functionality of GitHub Updater.
 * Version: 0.1.0
 * Author: Andy Fragen
 * License: GNU General Public License v2
 * License URI: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * Bitbucket Plugin URI: https://bitbucket.org/afragen/github-updater-scale
 * Requires WP: 4.0
 * Requires PHP: 5.4
 */

add_filter( 'github_updater_run_at_scale', '__return_true' );
